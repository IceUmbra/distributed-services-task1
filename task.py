import sys

res = ", ".join(set(map(lambda e: e.strip(), sys.argv[1:])))
with open("result.txt", "w") as f:
    f.write(res)
